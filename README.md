
# DeepOpinion Challenge

This is a solution to the challenge given for the Snr Software Engineer role. The application was developed using Sanic python web framework.


## Run tests

```bash
  make test
```


## Run server

```bash
  make run
```

## Endpoints
Check the `deepopinion_challenge.postman_collection.json` collection for requests.