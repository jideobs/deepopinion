import uuid
from typing import List, Callable, Dict

from sqlalchemy import select, Column, update
from sqlalchemy.orm import class_mapper, joinedload
from sqlalchemy.exc import IntegrityError

from main.exceptions import DataRelationIdError
from main.models import TrainingData, Tag, TrainingDataTag


def tag_obj_to_dict(tag: Tag) -> Dict:
    return {
        'id': str(tag.id),
        'aspect': tag.aspect,
        'sentiment': tag.sentiment,
        'created_datetime': str(tag.created_datetime),
        'updated_datetime': str(tag.updated_datetime)
    }


def data_obj_to_dict(data: TrainingData, tags: List[Dict]) -> Dict:
    return {
        'id': str(data.id),
        'text': data.text,
        'tags': tags,
        'created_datetime': str(data.created_datetime),
        'updated_datetime': str(data.updated_datetime)
    }


def to_dict(training_data: List[TrainingData]) -> List[Dict]:
    data = []
    for row in training_data:
        tags = []
        for tag in row.tags:
            tags.append(tag_obj_to_dict(tag))
        data.append(data_obj_to_dict(row, tags))
    return data


class TrainingDataRepository:
    def __init__(self, get_db_session: Callable):
        self.get_db_session = get_db_session

    async def insert_data(self, mappings: List[Dict]) -> None:
        async with self.get_db_session() as session:
            await session.run_sync(lambda sess: sess.bulk_insert_mappings(TrainingData, mappings))
            await session.commit()

    async def tag_data(self, data_id: uuid.UUID, tag_mappings: List[Dict]) -> None:
        async with self.get_db_session() as session:
            training_data_tags = []
            for tag in tag_mappings:
                tag_id = uuid.uuid4()
                tag['id'] = tag_id
                training_data_tags.append({'training_data_id': data_id, 'tag_id': tag_id})
            await session.run_sync(lambda sess: sess.bulk_insert_mappings(Tag, tag_mappings))
            try:
                await session.run_sync(lambda sess: sess.bulk_insert_mappings(TrainingDataTag, training_data_tags))
                await session.commit()
            except IntegrityError as e:
                raise DataRelationIdError(e)

    async def update_tag(self, tag_id: uuid.UUID, tag: Dict) -> None:
        async with self.get_db_session() as session:
            statement = update(Tag).where(Tag.id == tag_id).values(**tag)
            await session.run_sync(lambda sess: sess.execute(statement))
            await session.commit()

    async def get_tags(self, col: Column) -> List[Tag]:
        async with self.get_db_session() as session:
            statement = select(col)
            result = await session.run_sync(lambda sess: sess.execute(statement).all())
        return [row[0] for row in result]

    async def get_available_aspects(self) -> List[Tag]:
        return await self.get_tags(Tag.aspect)

    async def get_available_sentiments(self) -> List[Tag]:
        return await self.get_tags(Tag.sentiment)

    async def get_data(self) -> List[TrainingData]:
        async with self.get_db_session() as session:
            result = await session.run_sync(
                lambda sess: sess.query(TrainingData).options(joinedload(TrainingData.tags)).all())
        return result
