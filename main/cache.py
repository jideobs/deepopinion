from typing import Callable, Any

from aiocache import Cache
from aiocache.serializers import PickleSerializer

from main import ApplicationConfig


if ApplicationConfig.is_test():
    _cache = Cache(
        Cache.MEMORY,
    )
else:
    _cache = Cache(
        Cache.REDIS,
        endpoint=ApplicationConfig.REDIS_HOST,
        port=ApplicationConfig.REDIS_PORT,
        serializer=PickleSerializer(),
        namespace='deepopinion_cache'
    )


def cache(seconds: int) -> Callable:
    def wrapper(handler) -> Callable:
        async def inner(cls, request, *args, **kwargs) -> Any:
            cache_key = f"{request.url}"
            cached_data = await _cache.get(cache_key)
            if cached_data is not None:
                return cached_data

            response_ = await handler(cls, request, *args, **kwargs)
            await _cache.set(cache_key, response_, ttl=seconds)
            return response_

        return inner

    return wrapper
