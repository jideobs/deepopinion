from contextlib import asynccontextmanager

from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker

from main.config import ApplicationConfig

engine = create_async_engine(ApplicationConfig.SQLALCHEMY_DATABASE_URI, echo=True)
_async_sessionmaker = async_sessionmaker(engine, expire_on_commit=False)


@asynccontextmanager
async def get_session():
    session = _async_sessionmaker()
    yield session
    await session.close()
