from main.tasks import TrainingDataFileProcessor
from main.services import TrainingDataService
from main.db import get_session
from main.repository import TrainingDataRepository
from main.storage import get_storage

storage = get_storage()
data_repository = TrainingDataRepository(get_session)
data_service = TrainingDataService(data_repository, storage)
data_processor = TrainingDataFileProcessor(storage, data_repository)
