import uuid
from datetime import datetime

from sqlalchemy import Column, String, DateTime, ForeignKey, UUID
from sqlalchemy.orm import declarative_base, relationship

Base = declarative_base()


class BaseModel(Base):
    __abstract__ = True
    id = Column(UUID, primary_key=True, default=uuid.uuid4)
    created_datetime = Column(DateTime, default=datetime.utcnow)
    updated_datetime = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)


class TrainingData(BaseModel):
    __tablename__ = "training_data"

    text = Column(String())
    tags = relationship("Tag", secondary='training_data_tags', back_populates='training_data')


class Tag(BaseModel):
    __tablename__ = "tags"

    aspect = Column(String())
    sentiment = Column(String())
    training_data = relationship("TrainingData", secondary='training_data_tags', back_populates='tags')


class TrainingDataTag(BaseModel):
    __tablename__ = "training_data_tags"

    training_data_id = Column(ForeignKey("training_data.id"))
    tag_id = Column(ForeignKey("tags.id"))
    tag = relationship("Tag")
