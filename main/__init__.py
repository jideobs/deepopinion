from celery import Celery
from sanic import Sanic

from main import providers
from main.config import ApplicationConfig
from main.error_handler import CustomErrorHandler
from main.services import TrainingDataService
from main.tasks import TrainingDataFileProcessor
from main.views import TrainingDataView, TagView, TrainingDataDownloadView


celery_app = Celery(f'{ApplicationConfig.NAME}_tasks')
celery_app.conf.update(ApplicationConfig.CELERY_CONFIG)

_data_processor = celery_app.register_task(providers.data_processor)


def create_app(data_service: TrainingDataService, data_processor: TrainingDataFileProcessor) -> Sanic:
    app = Sanic(
        ApplicationConfig.NAME,
        config=ApplicationConfig(),
        error_handler=CustomErrorHandler())

    # Setup routes.
    app.add_route(
        TrainingDataView.as_view(data_service, data_processor),
        '/data',
        methods=["POST", "GET"],
    )

    tag_view = TagView.as_view(data_service)
    app.add_route(
        tag_view,
        '/data/tags/aspects',
        methods=['GET']
    )
    app.add_route(
        tag_view,
        '/data/tags/sentiments',
        methods=['GET']
    )
    app.add_route(
        tag_view,
        '/data/<data_id:uuid>/tags',
        methods=['POST']
    )
    app.add_route(
        tag_view,
        '/data/<data_id:uuid>/tags/<tag_id:uuid>',
        methods=['PUT']
    )

    training_data_download_view = TrainingDataDownloadView.as_view(data_service)
    app.add_route(
        training_data_download_view,
        '/data/download/csv',
        methods=['GET']
    )
    app.add_route(
        training_data_download_view,
        '/data/download/xlsx',
        methods=['GET']
    )

    return app


def create_app_prod() -> Sanic:
    """Create Sanic app with actual production database connection and storage."""
    return create_app(providers.data_service, _data_processor)
