import time
from typing import Any
import aiofiles

from sanic.request import File

from main.config import ApplicationConfig


class Storage:
    def save(self, uploaded_file) -> Any:
        raise NotImplemented

    def get(self, file_location: Any) -> Any:
        raise NotImplemented


class MemoryStorage(Storage):
    def __init__(self):
        self._files = []

    async def save(self, uploaded_file) -> Any:
        self._files.append(uploaded_file)
        return len(self._files) - 1

    async def get(self, file_location: Any) -> Any:
        try:
            return self._files[file_location]
        except IndexError:
            return None


class PersistentStorage(Storage):
    async def save(self, uploaded_file: File) -> Any:
        filename = f'{ApplicationConfig.FILE_UPLOAD_DIR}/{time.time_ns()}-{uploaded_file.name}'
        async with aiofiles.open(filename, 'wb') as f:
            await f.write(uploaded_file.body)
        await f.close()

        return filename

    async def get(self, filename: str) -> Any:
        return open(filename, 'r')


def get_storage() -> Storage:
    """Factory method to get storage based on environment."""
    if ApplicationConfig.is_test():
        return MemoryStorage()
    else:
        return PersistentStorage()
