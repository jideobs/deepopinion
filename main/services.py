from typing import Any, List, Dict
from uuid import UUID

from sanic.request import File

from main.repository import TrainingDataRepository, to_dict
from main.storage import Storage


class TrainingDataService:
    def __init__(self, data_repository: TrainingDataRepository, storage: Storage):
        self.data_repository = data_repository
        self.storage = storage

    async def save_data_file(self, uploaded_file: File) -> Any:
        return await self.storage.save(uploaded_file)

    async def get_training_data(self) -> List[Dict]:
        result = await self.data_repository.get_data()
        return to_dict(result)

    async def get_all_aspects(self) -> List[str]:
        return await self.data_repository.get_available_aspects()

    async def get_all_sentiments(self) -> List[str]:
        return await self.data_repository.get_available_sentiments()

    async def add_data_tags(self, data_id: UUID, tags: List[Dict]) -> None:
        await self.data_repository.tag_data(data_id, tags)

    async def update_tag(self, tag_id: UUID, tag_update: Dict) -> None:
        await self.data_repository.update_tag(tag_id, tag_update)

    async def stream_data(self):
        data = await self.data_repository.get_data()

        # Yield the header
        yield 'id,text,created_datetime,updated_datetime,aspects,sentiments\n'
        for row in data:
            entry = f'{row.id},{row.text},{row.created_datetime},{row.updated_datetime},'
            aspects = ''
            sentiments = ''
            for tag in row.tags:
                aspects += f'{tag.aspect} '
                sentiments += f'{tag.sentiment} '
            entry += f'{aspects.strip()},{sentiments.strip()}\n'
            yield entry
