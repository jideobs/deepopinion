from datetime import timedelta, datetime
from uuid import UUID

from sanic.request import Request
from sanic.response import HTTPResponse, json, ResponseStream
from sanic.views import HTTPMethodView

from main.cache import cache
from main.exceptions import DataRelationIdError
from main.services import TrainingDataService
from main.tasks import TrainingDataFileProcessor
from main.validators import validate_file_type, is_tag_valid, CSV_MIME_TYPE, EXCEL_MIME_TYPE


class TrainingDataView(HTTPMethodView):
    def __init__(self, data_service: TrainingDataService, data_file_processor: TrainingDataFileProcessor):
        self.data_file_processor = data_file_processor
        self.data_service = data_service

    @cache(60)
    async def get(self, request: Request) -> HTTPResponse:
        """Fetch stored training data"""
        training_data = await self.data_service.get_training_data()
        return json(training_data)

    async def post(self, request: Request) -> HTTPResponse:
        """Handle upload of unlabelled data"""
        data_file = request.files.get('data')
        if data_file is None:
            return json({"message": "invalid request, no file uploaded"}, status=400)

        if not validate_file_type(data_file.type):
            return json({"message": "file uploaded invalid, allowed types (csv, xlsx)"}, status=400)

        file_location = await self.data_service.save_data_file(data_file)

        # Start data processor in 5minutes.
        self.data_file_processor.apply_async(args=[file_location, data_file.type], countdown=60)
        return json({"message": "Successfully uploaded data"}, status=201)


class TagView(HTTPMethodView):
    def __init__(self, data_service: TrainingDataService):
        self.data_service = data_service

    @cache(60)
    async def get(self, request: Request) -> HTTPResponse:
        # get the tag label it should fetch
        request_paths = request.path.split('/')
        tag_label_to_fetch = request_paths[-1]
        if tag_label_to_fetch == 'aspects':
            tags = await self.data_service.get_all_aspects()
        else:
            tags = await self.data_service.get_all_sentiments()
        return json(tags)

    async def post(self, request: Request, data_id: UUID) -> HTTPResponse:
        # validate tags
        for idx, tag in enumerate(request.json):
            if not is_tag_valid(tag):
                return json({'message': f'invalid data as an item {idx} does not contain either an aspect and sentiment'}, status=400)

        try:
            await self.data_service.add_data_tags(data_id, request.json)
        except DataRelationIdError:
            return json({"message": "invalid data id passed"}, status=400)
        return json({"message": "successfully tagged data"}, status=201)

    async def put(self, request: Request, data_id: UUID, tag_id: UUID) -> HTTPResponse:
        if not is_tag_valid(request.json):
            return json({'message': f'invalid data tag, does not contain either an aspect and sentiment'},
                        status=400)
        await self.data_service.update_tag(tag_id, request.json)
        return json({'message': 'successfully updated tag'})


class TrainingDataDownloadView(HTTPMethodView):
    def __init__(self, data_service: TrainingDataService):
        self.data_service = data_service

    async def stream_it(self, response):
        async for chunk in self.data_service.stream_data():
            await response.write(chunk)

    async def get(self, request: Request) -> ResponseStream:
        request_paths = request.path.split('/')
        file_type = request_paths[-1]
        filename, content_type = ('data.csv', CSV_MIME_TYPE) if file_type == 'csv' else ('data.xlsx', EXCEL_MIME_TYPE)
        headers = {'Content-Disposition': f'attachment; filename="{filename}"'}
        return ResponseStream(
            self.stream_it,
            headers=headers,
            content_type=content_type)
