class DataRelationIdError(Exception):
    """This is raised when there is an IntegrityError from ORM."""
    pass
