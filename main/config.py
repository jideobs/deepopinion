import os

from sanic.config import Config


DB_USER = os.environ['DB_USER']
DB_PASSWORD = os.environ['DB_PASSWORD']
DB_NAME = os.environ['DB_NAME']
DB_HOST = os.environ['DB_HOST']
DB_PORT = os.environ['DB_PORT']


class ApplicationConfig(Config):
    """Application configuration"""
    NAME = 'DeepOpinion'
    BASE_DIR = os.path.dirname(os.path.dirname(__file__))
    ENVIRONMENT = os.environ['ENVIRONMENT']
    FALLBACK_ERROR_FORMAT = 'json'
    Debug = os.environ['DEBUG']

    ALEMBIC_CONFIG_FILE = f'{BASE_DIR}/alembic.ini'
    SQLALCHEMY_DATABASE_URI = f'postgresql+asyncpg://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}'

    FILE_UPLOAD_DIR = f'{BASE_DIR}/{os.environ["UPLOADS_DIR"]}'

    REDIS_HOST = os.environ['REDIS_HOST']
    REDIS_PORT = os.environ['REDIS_PORT']

    # Celery
    CELERY_CONFIG = {
        'broker_url': f'redis://{REDIS_HOST}:{REDIS_PORT}/0',
        'broker_transport_options': {'visibility_timeout': 3600},
        'result_backend': f'redis://{REDIS_HOST}:{REDIS_PORT}/0',
        'result_serializer': 'pickle',
        'result_backend_transport_options': {
            'retry_policy': {
                'timeout': 5.0
            }
        }
    }

    @classmethod
    def is_test(cls):
        return cls.ENVIRONMENT == 'test'
