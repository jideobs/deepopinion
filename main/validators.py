from typing import Dict

CSV_MIME_TYPE = 'text/csv'
EXCEL_MIME_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'

ALLOWED_FILE_TYPES = [CSV_MIME_TYPE, EXCEL_MIME_TYPE]


def validate_file_type(file_type: str) -> bool:
    return file_type in ALLOWED_FILE_TYPES


def is_tag_valid(tag: Dict) -> bool:
    return 'aspect' in tag and 'sentiment' in tag
