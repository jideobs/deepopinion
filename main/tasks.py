import asyncio
import csv
from typing import Any

import celery
import openpyxl

from main.repository import TrainingDataRepository
from main.storage import Storage


class TrainingDataFileProcessor(celery.Task):
    def __init__(self, file_storage: Storage, data_repository: TrainingDataRepository):
        self.storage = file_storage
        self.data_repository = data_repository

    def run(self, file_location: Any, filetype: str, *args, **kwargs) -> None:
        """Process uploaded unlabelled data into the database."""
        data_file = asyncio.run(self.storage.get(file_location))
        training_data_mappings = []
        if filetype == 'text/csv':
            csvreader = csv.reader(data_file)
            for row in csvreader:
                training_data_mappings.append({'text': row[0]})
        else:
            wb = openpyxl.load_workbook(file_location)
            for row in wb.active.iter_rows():
                training_data_mappings.append({'text': row[0].value})

        asyncio.run(self.data_repository.insert_data(training_data_mappings))
