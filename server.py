from main import create_app_prod
from main.config import ApplicationConfig

application = create_app_prod()


if __name__ == '__main__':
    application.run(host='0.0.0.0', port=8000, dev=ApplicationConfig.Debug, debug=ApplicationConfig.Debug)
