import asyncio
import json
from typing import Callable

import pytest
from mockito import mock, verify
from sanic import Sanic

from main import create_app
from main.tasks import TrainingDataFileProcessor
from main.services import TrainingDataService


@pytest.fixture
def training_data_service() -> TrainingDataService:
    return mock(TrainingDataService)


@pytest.fixture
def data_processor() -> TrainingDataFileProcessor:
    return mock(TrainingDataFileProcessor)


@pytest.fixture
def app(training_data_service: TrainingDataService, data_processor: TrainingDataFileProcessor) -> Sanic:
    return create_app(training_data_service, data_processor)


@pytest.mark.asyncio
async def test_get_training_data(training_data_service: TrainingDataService, app: Sanic, when: Callable) -> None:
    # Arrange:
    future = asyncio.Future()
    future.set_result([])
    when(training_data_service).get_training_data(...).thenReturn(future)

    # Act:
    request, response = await app.asgi_client.get("/data")

    # Assert:
    assert request.method.lower() == 'get'
    assert response.body == b"[]"
    assert response.status == 200

    verify(training_data_service, 1).get_training_data()


@pytest.mark.asyncio
async def test_post_training_data_tag__should_return_status_201(training_data_service: TrainingDataService, when: Callable, app: Sanic) -> None:
    # Arrange:
    future = asyncio.Future()
    future.set_result(None)
    when(training_data_service).add_data_tags(...).thenReturn(future)

    # Act:
    request, response = await app.asgi_client.post(
        "/data/e0817b88-c2fd-4efa-bcaf-0d9143e203eb/tags", data=json.dumps([{'aspect': 'Room', 'sentiment': 'NEG'}]))

    # Assert:
    assert request.method.lower() == 'post'
    assert response.body == b'{"message":"successfully tagged data"}'
    assert response.status == 201

    verify(training_data_service, 1).add_data_tags(...)


@pytest.mark.asyncio
async def test_put_update_tag__should_return_status_200(training_data_service: TrainingDataService, when: Callable, app: Sanic) -> None:
    # Arrange:
    future = asyncio.Future()
    future.set_result(None)
    when(training_data_service).update_tag(...).thenReturn(future)

    # Act:
    request, response = await app.asgi_client.put(
        "/data/e0817b88-c2fd-4efa-bcaf-0d9143e203eb/tags/e0817b88-c2fd-4efa-bcaf-0d9143e203e1", data=json.dumps(
            {'aspect': 'Room', 'sentiment': 'NEG'}
        )
    )

    # Assert:
    assert request.method.lower() == "put"
    assert response.body == b'{"message":"successfully updated tag"}'
    assert response.status == 200


@pytest.mark.asyncio
async def test_get_all_available_aspects__should_return_list_of_tags(
        app: Sanic, training_data_service: TrainingDataService, when: Callable) -> None:
    # Arrange:
    aspects = ['Room', 'Staff']
    future = asyncio.Future()
    future.set_result(aspects)
    when(training_data_service).get_all_aspects(...).thenReturn(future)

    # Act:
    request, response = await app.asgi_client.get('/data/tags/aspects')

    # Assert:
    assert request.method.lower() == "get"
    assert response.body == b'["Room","Staff"]'
    assert response.status == 200


@pytest.mark.asyncio
async def test_get_all_available_sentiments__should_return_list_of_sentiments(
        app: Sanic, training_data_service: TrainingDataService, when: Callable) -> None:
    # Arrange:
    sentiments = ['POS', 'NEG', 'NEU']
    future = asyncio.Future()
    future.set_result(sentiments)
    when(training_data_service).get_all_sentiments(...).thenReturn(future)

    # Act:
    request, response = await app.asgi_client.get('/data/tags/sentiments')

    # Assert:
    assert request.method.lower() == "get"
    assert response.body == b'["POS","NEG","NEU"]'
    assert response.status == 200
