import asyncio
import uuid
from typing import Callable, List

import pytest
from mockito import mock, verify

from main import TrainingDataService
from main.models import TrainingData, Tag
from main.repository import TrainingDataRepository
from main.storage import MemoryStorage
from main.storage import Storage


@pytest.fixture
def data_repository() -> TrainingDataRepository:
    return mock(TrainingDataRepository)


@pytest.fixture
def storage() -> Storage:
    return MemoryStorage()


@pytest.fixture
def list_of_training_data() -> List[TrainingData]:
    return [TrainingData(id=uuid.uuid4(), text='Sample text')]


@pytest.fixture
def data_service(storage: Storage, data_repository: TrainingDataRepository):
    return TrainingDataService(data_repository, storage)


@pytest.mark.asyncio
async def test_get_training_data(
        when: Callable, data_service: TrainingDataService, data_repository: TrainingDataRepository, list_of_training_data: List[TrainingData]) -> None:
    # Arrange:
    future = asyncio.Future()
    future.set_result(list_of_training_data)
    when(data_repository).get_data().thenReturn(future)

    # Act:
    _list_of_training_data = await data_service.get_training_data()

    # Assert:
    verify(data_repository, 1).get_data()

    expected_list_of_data = [{
        'id': str(list_of_training_data[0].id),
        'text': list_of_training_data[0].text,
        'tags': [],
        'created_datetime': 'None',
        'updated_datetime': 'None'}]
    assert expected_list_of_data == _list_of_training_data


@pytest.mark.asyncio
async def test_get_all_aspects(
        when: Callable, data_service: TrainingDataService, data_repository: TrainingDataRepository) -> None:
    # Arrange:
    future = asyncio.Future()
    future.set_result([Tag(id=uuid.uuid4(), aspect='Staff')])
    when(data_repository).get_available_aspects().thenReturn(future)

    # Act:
    await data_service.get_all_aspects()

    # Assert:
    verify(data_repository, 1).get_available_aspects()


@pytest.mark.asyncio
async def test_get_all_sentiments(
        when: Callable, data_service: TrainingDataService, data_repository: TrainingDataRepository) -> None:
    # Arrange:
    future = asyncio.Future()
    future.set_result([Tag(id=uuid.uuid4(), aspect='Staff')])
    when(data_repository).get_available_sentiments().thenReturn(future)

    # Act:
    await data_service.get_all_sentiments()

    # Assert:
    verify(data_repository, 1).get_available_sentiments()


@pytest.mark.asyncio
async def test_update_tag(
        when: Callable, data_service: TrainingDataService, data_repository: TrainingDataRepository) -> None:
    # Arrange:
    future = asyncio.Future()
    future.set_result(None)
    when(data_repository).update_tag(...).thenReturn(future)

    # Act:
    await data_service.update_tag(uuid.uuid4(), {'aspect': 'ROOM', 'sentiment': 'NEG'})

    # Assert:
    verify(data_repository, 1).update_tag(...)
