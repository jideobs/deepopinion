import uuid
from contextlib import asynccontextmanager
from typing import Callable
from sqlalchemy.engine.row import Row

import pytest
from alembic.config import Config
from sqlalchemy import select
from sqlalchemy.ext.asyncio import create_async_engine, AsyncEngine, AsyncSession, async_sessionmaker
from alembic import command

from main import ApplicationConfig
from main.repository import TrainingDataRepository
from main.models import TrainingData, Tag


@pytest.fixture
def engine() -> AsyncEngine:
    return create_async_engine(ApplicationConfig.SQLALCHEMY_DATABASE_URI, echo=True)


@pytest.fixture
def session(engine) -> async_sessionmaker:
    _async_sessionmaker = async_sessionmaker(engine, expire_on_commit=False)
    return _async_sessionmaker()

@pytest.fixture
def alembic_config() -> Config:
    return Config(ApplicationConfig.ALEMBIC_CONFIG_FILE)


@pytest.fixture(autouse=True, scope='function')
def db_setup_and_teardown(alembic_config) -> None:
    command.upgrade(config=alembic_config, revision='head')
    yield
    command.downgrade(config=alembic_config, revision='-1')


@pytest.fixture
def db_session(session) -> (Callable, AsyncSession):
    @asynccontextmanager
    async def get_session():
        yield session
        await session.close()
    return get_session, session


@pytest.fixture
async def training_data(db_session: (Callable, AsyncSession)) -> TrainingData:
    data_to_be_inserted = [
        {'text': 'this is a test'}
    ]
    _get_session, session = db_session
    training_data_repository = TrainingDataRepository(_get_session)
    await training_data_repository.insert_data(data_to_be_inserted)
    result = await training_data_repository.get_data()
    training_data = result[0]

    return training_data


async def tag_data(training_data: TrainingData, db_session: (Callable, AsyncSession)) -> None:
    tags = [
        {'aspect': 'Room', 'sentiment': 'NEG'},
        {'aspect': 'Staff', 'sentiment': "POS"}
    ]
    _get_session, session = db_session
    training_data_repository = TrainingDataRepository(_get_session)
    await training_data_repository.tag_data(training_data.id, tags)


@pytest.mark.asyncio
async def test_insert_bulk_training_data(db_session: (Callable, AsyncSession)) -> None:
    # Arrange:
    data_to_be_inserted = [
        {'text': 'this is a test'}
    ]
    _get_session, session = db_session
    training_data = TrainingDataRepository(_get_session)
    
    # Act:
    await training_data.insert_data(data_to_be_inserted)
    result = await training_data.get_data()

    # Assert:
    assert len(result) == 1


@pytest.mark.asyncio
async def test_tag_data(db_session: (Callable, AsyncSession), training_data: TrainingData) -> None:
    # Arrange:
    tags = [
        {'aspect': 'Room', 'sentiment': 'NEG'},
        {'aspect': 'Staff', 'sentiment': "POS"}
    ]
    _get_session, session = db_session
    training_data_repository = TrainingDataRepository(_get_session)

    # Act:
    await training_data_repository.tag_data(training_data.id, tags)
    result = await training_data_repository.get_data()
    updated_training_data = result[0]

    # Assert:
    assert updated_training_data.tags is not None
    assert len(updated_training_data.tags) == 2


@pytest.mark.asyncio
async def test_get_available_aspects(training_data: TrainingData, db_session: (Callable, AsyncSession)) -> None:
    # Arrange:
    await tag_data(training_data, db_session)
    _get_session, session = db_session
    training_data_repository = TrainingDataRepository(_get_session)

    # Act
    result = await training_data_repository.get_available_aspects()

    # Assert
    assert len(result) == 2


@pytest.mark.asyncio
async def test_get_available_sentiments(training_data: TrainingData, db_session: (Callable, AsyncSession)) -> None:
    # Arrange:
    await tag_data(training_data, db_session)
    _get_session, session = db_session
    training_data_repository = TrainingDataRepository(_get_session)

    # Act
    result = await training_data_repository.get_available_sentiments()

    # Assert
    assert len(result) == 2


@pytest.mark.asyncio
async def test_update_tag(db_session: (Callable, AsyncSession), training_data: TrainingData):
    # Arrange:
    tags = [
        {'id': uuid.uuid4(), 'aspect': 'Room', 'sentiment': 'NEG'},
        {'id': uuid.uuid4(), 'aspect': 'Staff', 'sentiment': "POS"}
    ]
    _get_session, session = db_session
    training_data_repository = TrainingDataRepository(_get_session)
    await training_data_repository.tag_data(training_data.id, tags)

    # Act:
    await training_data_repository.update_tag(
        tags[0]['id'], {'aspect': 'Room', 'sentiment': 'POS'})

    # Assert:
    async with _get_session() as session:
        statement = select(Tag).where(Tag.id == tags[0]['id'])
        result = await session.run_sync(lambda sess: sess.execute(statement).fetchone())

        assert result[0].sentiment == 'POS'
