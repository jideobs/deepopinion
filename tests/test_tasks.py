import asyncio
import io
from typing import Callable, Coroutine, Any
from unittest.mock import AsyncMock

import pytest
from mockito import mock, verify

from main.repository import TrainingDataRepository
from main.tasks import TrainingDataFileProcessor
from main.storage import Storage, MemoryStorage


@pytest.fixture
def file_storage() -> Storage:
    return mock(MemoryStorage)


@pytest.fixture
def data_repository() -> TrainingDataRepository:
    return mock(TrainingDataRepository)


@pytest.fixture
def data_processor(file_storage: Storage, data_repository: TrainingDataRepository) -> TrainingDataFileProcessor:
    return TrainingDataFileProcessor(file_storage, data_repository)


def test_upload_training_data_file_processor__csv_file(
        when: Callable, file_storage: Storage, data_repository: TrainingDataRepository,
        data_processor: TrainingDataFileProcessor):
    mock_ = AsyncMock()
    mock_.return_value = io.StringIO("The Room was terrible but the staff was very friendly.")
    when(file_storage).get(...).thenReturn(mock_())

    insert_data_mock = AsyncMock()
    insert_data_mock.return_value = None
    when(data_repository).insert_data(...).thenReturn(insert_data_mock())

    # Act:
    data_processor.run("test_file.csv", "text/csv")

    # Assert:
    verify(file_storage, 1).get(...)
    verify(data_repository, 1).insert_data(...)
