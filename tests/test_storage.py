import io

import pytest

from main.storage import MemoryStorage


@pytest.mark.asyncio
async def test_save_file() -> None:
    # Arrange:
    mem_storage = MemoryStorage()
    file = io.StringIO("This is a test file")

    # Act:
    file_location = await mem_storage.save(file)

    # Assert:
    assert file_location == 0


@pytest.mark.asyncio
async def test_get_file() -> None:
    # Arrange:
    mem_storage = MemoryStorage()
    file = io.StringIO("This is a test file")

    # Act:
    file_location = await mem_storage.save(file)
    fetched_file = await mem_storage.get(file_location)

    # Assert:
    assert file == fetched_file
