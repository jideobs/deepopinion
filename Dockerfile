FROM python:3.11

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN apt update \
    && apt install -y python3-dev build-essential \
    && python3 -m pip install --upgrade setuptools pip wheel \
    && pip install --no-cache-dir -r ./requirements.txt

CMD make applymigrations && make startceleryworker && make run
