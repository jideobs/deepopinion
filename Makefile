build:
	docker-compose build

test:
	docker-compose run --service-ports deepopinion_test

migration:
	alembic revision --autogenerate -m "$(m)"

applymigrations:
	alembic upgrade head

startceleryworker:
	celery -A main:celery_app worker -l INFO --detach

startserver:
	sanic server.application --host 0.0.0.0 --dev

run:
	docker-compose run --service-ports deepopinion_run
